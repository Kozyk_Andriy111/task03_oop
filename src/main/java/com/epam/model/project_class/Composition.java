package com.epam.model.project_class;

public class Composition extends NYDecoration {
    private int width;
    private int length;
    private int height;
    private String description;

    public Composition(int order, String name, int price, boolean availability, int width, int length, int height, String description, String purpose) {
        super(order, name, price, availability);
        this.width = width;
        this.length = length;
        this.height = height;
        this.description = description;
        super.setPurpose("For flat");
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nWidth: " + width +
                "\nLength: " + length +
                "\nHeight: " + height +
                "\nDescription: " + description;
    }
}
