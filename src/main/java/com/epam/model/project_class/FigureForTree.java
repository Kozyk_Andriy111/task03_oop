package com.epam.model.project_class;

public class FigureForTree extends HangingDecorationForTree {
    private String description;

    public FigureForTree(int order, String name, int price, boolean availability, int numberOfPack, String size, String description) {
        super(order, name, price, availability, numberOfPack, size);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nDescription: " + description;
    }
}
