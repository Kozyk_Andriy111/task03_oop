package com.epam.model.project_class;

public class GarlandForFlat extends Garland {
    private String description;
    private String formOfLamp;

    public GarlandForFlat(int order, String name, int price, boolean availability, int numberOfLamp, int numberOfColors, String description, String formOfLamp) {
        super(order, name, price, availability, numberOfLamp, numberOfColors);
        this.description = description;
        this.formOfLamp = formOfLamp;
        super.setPurpose("For flat");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFormOfLamp() {
        return formOfLamp;
    }

    public void setFormOfLamp(String formOfLamp) {
        this.formOfLamp = formOfLamp;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nDescription: " + description +
                "\nForm of lamp: " + formOfLamp;
    }
}
