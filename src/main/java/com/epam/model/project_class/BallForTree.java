package com.epam.model.project_class;

public class BallForTree extends HangingDecorationForTree {
    private String color;

    public BallForTree(int order, String name, int price, boolean availability, int numberOfPack, String size, String color) {
        super(order, name, price, availability, numberOfPack, size);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nColor: " + color;
    }
}
