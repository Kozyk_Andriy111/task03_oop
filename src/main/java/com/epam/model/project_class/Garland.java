package com.epam.model.project_class;

public abstract class Garland extends NYDecoration {
    private int numberOfLamp;
    private int numberOfColors;

    public Garland(int order, String name, int price, boolean availability, int numberOfLamp, int numberOfColors) {
        super(order, name, price, availability);
        this.numberOfLamp = numberOfLamp;
        this.numberOfColors = numberOfColors;
    }

    public int getNumberOfLamp() {
        return numberOfLamp;
    }

    public void setNumberOfLamp(int numberOfLamp) {
        this.numberOfLamp = numberOfLamp;
    }

    public int getNumberOfColors() {
        return numberOfColors;
    }

    public void setNumberOfColors(int numberOfColors) {
        this.numberOfColors = numberOfColors;
    }
}
