package com.epam.model.project_class;

import java.util.Scanner;

public abstract class NYDecoration {
    Scanner in = new Scanner(System.in);
    private int order;
    private String name;
    private int price;
    private boolean availability;
    private String purpose;
    private int amountOfProduct;

    public NYDecoration(int order, String name, int price, boolean availability) {
        this.order = order;
        this.name = name;
        this.price = price;
        this.availability = availability;
    }


    public int getOrder() {
        return order;
    }

    public void setOrder() {
        order = in.nextInt();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPurpose() {
        return purpose;
    }

    public int getAmountOfProduct() {
        return amountOfProduct;
    }

    public void setAmountOfProduct() {
        amountOfProduct = in.nextInt();
    }

    @Override
    public String toString() {
        return "Name: " + name +
                "\nPrice: " + price +
                "\nAvailability: " + availability;
    }
}
