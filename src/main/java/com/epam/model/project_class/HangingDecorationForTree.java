package com.epam.model.project_class;

public abstract class HangingDecorationForTree extends NYDecoration {
    private int numberOfPack;
    private String size;

    public HangingDecorationForTree(int order, String name, int price, boolean availability, int numberOfPack, String size) {
        super(order, name, price, availability);
        this.numberOfPack = numberOfPack;
        this.size = size;
        super.setPurpose("For tree");
    }

    public int getNumberOfPack() {
        return numberOfPack;
    }

    public void setNumberOfPack(int numberOfPack) {
        this.numberOfPack = numberOfPack;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nNumber of pack: " + numberOfPack +
                "\nSize: " + size;
    }
}
