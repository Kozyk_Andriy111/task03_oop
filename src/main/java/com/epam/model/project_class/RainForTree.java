package com.epam.model.project_class;

public class RainForTree extends NYDecoration {
    private int length;
    private String color;

    public RainForTree(int order, String name, int price, boolean availability, int length, String color) {
        super(order, name, price, availability);
        this.length = length;
        this.color = color;
        super.setPurpose("For tree");
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nLength: " + length +
                "\nColor: " + color;
    }
}
