package com.epam.model.project_class;

public class GarlandForTree extends Garland {
    private int length;
    private boolean forTreeOnTheStreet;

    public GarlandForTree(int order, String name, int price, boolean availability, int numberOfLamp, int numberOfColors, int length, boolean forTreeOnTheStreet) {
        super(order, name, price, availability, numberOfLamp, numberOfColors);
        this.length = length;
        this.forTreeOnTheStreet = forTreeOnTheStreet;
        super.setPurpose("For tree");
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isForTreeOnTheStreet() {
        return forTreeOnTheStreet;
    }

    public void setForTreeOnTheStreet(boolean forTreeOnTheStreet) {
        this.forTreeOnTheStreet = forTreeOnTheStreet;
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nLength: " + length +
                "\nFor tree on the street: " + forTreeOnTheStreet;
    }
}
