package com.epam.model;

import com.epam.model.project_class.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Domain {
    private final int CONST1 = 1;
    private List<Object> objectList;
    private List<Object> buyList;
    private int totalSum = 0;

    Domain() {
        objectList = new LinkedList<>();
        buyList = new LinkedList<>();
        NYDecoration garland1 = new GarlandForFlat(1, "Garland type 1", 375, true, 225, 5, "Blind garland", "Default");
        NYDecoration garland2 = new GarlandForTree(2, "Garland type 2", 220, true, 150, 3, 10, false);
        NYDecoration ball1 = new BallForTree(3, "Ball for tree type 1", 50, true, 6, "Medium", "Green");
        NYDecoration figure1 = new FigureForTree(4, "Figure for tree type 1", 75, false, 1, "Small", "Rabbit");
        NYDecoration rain1 = new RainForTree(5, "Rain for tree type 1", 125, true, 15, "White");

        objectList.add(garland1);
        objectList.add(garland2);
        objectList.add(ball1);
        objectList.add(figure1);
        objectList.add(rain1);
    }

    public void printProductList() {
        printShortInformation((NYDecoration) objectList.get(0));
        printShortInformation((NYDecoration) objectList.get(1));
        printShortInformation((NYDecoration) objectList.get(2));
        printShortInformation((NYDecoration) objectList.get(3));
        printShortInformation((NYDecoration) objectList.get(4));
        System.out.println("Please, select number");
    }

    private void printShortInformation(NYDecoration nyDecoration) {
        System.out.println("===============================");
        System.out.println("Name: " + nyDecoration.getName());
        System.out.println("Price: " + nyDecoration.getPrice());
        System.out.println("SELECT: " + nyDecoration.getOrder());
        System.out.println("===============================");
    }

    public void printAllInfoProduct(int key) {
        System.out.println(objectList.get(key));
    }

    public void searchProductForFlat() {
        for (int i = 0; i < objectList.size(); i++) {
            if (forSearch((NYDecoration) objectList.get(i)).equals("For flat")) {
                printShortInformation((NYDecoration) objectList.get(i));
            }
        }
    }

    public void searchProductForTree() {
        for (int i = 0; i < objectList.size(); i++) {
            if (forSearch((NYDecoration) objectList.get(i)).equals("For tree")) {
                printShortInformation((NYDecoration) objectList.get(i));
            }
        }
    }

    private String forSearch(NYDecoration nyDecoration) {
        return nyDecoration.getPurpose();
    }

    public void buyProduct(int key) {
        buyList.add(objectList.get(key - CONST1));
        System.out.println("Enter amount of product: ");
        ((NYDecoration)objectList.get(key - CONST1)).setAmountOfProduct();
        System.out.println("This product added in basket");
    }

    private void productInBasket(NYDecoration nyDecoration) {
        System.out.println("\nYour basket: ");
        System.out.println("===============================");
        System.out.println("Name: " + nyDecoration.getName());
        System.out.println("Price: " + nyDecoration.getPrice());
        System.out.println("Amount: " + nyDecoration.getAmountOfProduct());
        System.out.println("Total: " + (nyDecoration.getPrice() * nyDecoration.getAmountOfProduct()));
        totalSum += nyDecoration.getPrice() * nyDecoration.getAmountOfProduct();
        System.out.println("===============================");
    }

    public void viewBasket() {
        for (int i = 0; i < buyList.size(); i++) {
            productInBasket((NYDecoration) buyList.get(i));
        }
        System.out.println("Total price: " + totalSum);
        System.out.println("Q - Back into main menu");
    }
}
