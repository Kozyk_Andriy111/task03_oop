package com.epam.view;

import com.epam.controller.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private final int CONST1 = 1;
    private Controller controller;
    private Map<String, String> mainMenu;
    private Map<String, Printable> methodsMainMenu;
    private Map<String, String> menu1;
    private Map<String, Printable> methodsMenu1;
    private Map<String, String> buyOrBackFor1;
    private Map<String, Printable> methodsBuyOrBackFor1;
    private Map<String, String> search;
    private Map<String, Printable> methodsSearch;
    private Map<String, String> menuSearch;
    private Map<String, Printable> methodsMenuSearch;
    private Map<String, String> buyOrBackForSearch;
    private Map<String, Printable> methodsBuyOrBackForSearch;
    private String keyMenu;
    private int key1;
    private String keyForSearch;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        mainMenu = new LinkedHashMap<>();
        mainMenu.put("1", " 1 - Print all product list");
        mainMenu.put("2", " 2 - Search product");
        mainMenu.put("3", " 3 - View basket");
        mainMenu.put("Q", " Q - Exit");

        methodsMainMenu = new LinkedHashMap<>();
        methodsMainMenu.put("1", this::pressButton1);
        methodsMainMenu.put("2", this::pressButton2);
        methodsMainMenu.put("3", this::pressButton3);
        methodsMainMenu.put("Q", this::pressExit);

        menu1 = new LinkedHashMap<>();
        menu1.put("1", " 1 - View all information about product");
        menu1.put("2", " 2 - Buy now!");
        menu1.put("Q", " Q - Back");

        methodsMenu1 = new LinkedHashMap<>();
        methodsMenu1.put("1", this::pressButton4);
        methodsMenu1.put("2", this::pressButton5);
        methodsMenu1.put("Q", this::mainMenu);

        menuSearch = new LinkedHashMap<>();
        menuSearch.put("1", " 1 - View all information about product");
        menuSearch.put("2", " 2 - Buy now!");
        menuSearch.put("Q", " Q - Back");

        methodsMenuSearch = new LinkedHashMap<>();
        methodsMenuSearch.put("1", this::pressButton6);
        methodsMenuSearch.put("2", this::pressButton5);
        methodsMenuSearch.put("Q", this::mainMenu);

        buyOrBackFor1 = new LinkedHashMap<>();
        buyOrBackFor1.put("1", " 1 - Buy");
        buyOrBackFor1.put("Q", " Q - Back");

        methodsBuyOrBackFor1 = new LinkedHashMap<>();
        methodsBuyOrBackFor1.put("1", this::pressButton5);
        methodsBuyOrBackFor1.put("Q", this::pressButton1);

        buyOrBackForSearch = new LinkedHashMap<>();
        buyOrBackForSearch.put("1", " 1 - Buy");
        buyOrBackForSearch.put("Q", " Q - Back");

        methodsBuyOrBackForSearch = new LinkedHashMap<>();
        methodsBuyOrBackForSearch.put("1", this::pressButton5);
        methodsBuyOrBackForSearch.put("Q", this::pressButton9);

        search = new LinkedHashMap<>();
        search.put("1", " 1 - For flat");
        search.put("2", " 2 - For tree");
        search.put("Q", " Q - Back");

        methodsSearch = new LinkedHashMap<>();
        methodsSearch.put("1", this::pressButton7);
        methodsSearch.put("2", this::pressButton8);
        methodsSearch.put("Q", this::mainMenu);
    }

    private void pressButton1() {
        controller.printProductList();
        key1 = input.nextInt();
        for (String str : menu1.values()) {
            System.out.println(str);
        }
        keyMenu = input.next().toUpperCase();
        methodsMenu1.get(keyMenu).print();
    }

    private void pressButton2() {
        for (String str : search.values()) {
            System.out.println(str);
        }
        keyForSearch = input.next().toUpperCase();
        methodsSearch.get(keyForSearch).print();
        key1 = input.nextInt();
        for (String str : menuSearch.values()) {
            System.out.println(str);
        }
        keyMenu = input.next().toUpperCase();
        methodsMenuSearch.get(keyMenu).print();
    }

    private void pressButton3() {
        controller.viewBasket();
        keyMenu = input.next();
        mainMenu();
    }

    private void pressButton4() {
        controller.printAllInfoProduct(key1 - CONST1);
        for (String str : buyOrBackFor1.values()) {
            System.out.println(str);
        }
        keyMenu = input.next().toUpperCase();
        methodsBuyOrBackFor1.get(keyMenu).print();
    }

    private void pressButton5() {
        controller.buyProduct(key1);
        mainMenu();
    }

    private void pressButton6() {
        controller.printAllInfoProduct(key1 - CONST1);
        for (String str : buyOrBackForSearch.values()) {
            System.out.println(str);
        }
        keyMenu = input.next().toUpperCase();
        methodsBuyOrBackForSearch.get(keyMenu).print();
    }

    private void pressButton7() {
        controller.searchProductForFlat();
    }

    private void pressButton8() {
        controller.searchProductForTree();
    }

    private void pressButton9() {
        if(keyForSearch.equals("1")) {
            pressButton7();
        } else if (keyForSearch.equals("2")) {
            pressButton8();
        }
        key1 = input.nextInt();
        for (String str : menu1.values()) {
            System.out.println(str);
        }
        keyMenu = input.next().toUpperCase();
        methodsMenu1.get(keyMenu).print();
    }

    private void pressExit() {
        System.exit(0);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : mainMenu.values()) {
            System.out.println(str);
        }
    }

    public void mainMenu() {
        String keyMenu;
        outputMenu();
        System.out.println("Please, select menu point.");
        keyMenu = input.next().toUpperCase();
        try {
            methodsMainMenu.get(keyMenu).print();
        } catch (Exception e) {
            System.out.println("You have an exception");
        }
    }
}