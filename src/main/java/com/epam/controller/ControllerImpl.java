package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

public class ControllerImpl implements Controller {
    private Model model;
    public ControllerImpl() {model = new BusinessLogic();
    }
    @Override
    public void printProductList() {
        model.printProductList();
    }

    @Override
    public void printAllInfoProduct(int key) {
        model.printAllInfoProduct(key);
    }

    @Override
    public void searchProductForTree() {
        model.searchProductForTree();
    }

    @Override
    public void searchProductForFlat() {
        model.searchProductForFlat();
    }

    @Override
    public void buyProduct(int key) {
        model.buyProduct(key);
    }

    @Override
    public void viewBasket() {
        model.viewBasket();
    }
}